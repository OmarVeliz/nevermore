package com.example.jake.proyecto3bimestre;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jake.proyecto3bimestre.Database.DbHelper;
import com.example.jake.proyecto3bimestre.bean.Notas;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotesActivity extends Fragment {

    private ImageButton btnFloat;
    private ListView notas;


    public NotesActivity() {
        // Required empty public constructor
    }

    final String[] datos = new String[] {"Titulo", "Descripcion"};
    ArrayList<Notas> lista = new ArrayList<Notas>();
    private AdaptadorNotas adaptador;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_notes, container, false);
        notas = (ListView)v.findViewById(R.id.listView);

        //Db

        DbHelper notes1 = new DbHelper(getActivity().getApplicationContext(), "DBNevermore", null, 1);
        SQLiteDatabase db = notes1.getWritableDatabase();

        //Llenar el listview

        Cursor cursor= db.rawQuery("SELECT title,description, id FROM Notes", null);
        Notas listaNotas;

        if (cursor.moveToFirst()){
            do {
                listaNotas = new Notas();
                listaNotas.setTitle(cursor.getString(0));
                listaNotas.setDescription(cursor.getString(1));
                listaNotas.setId(cursor.getInt(2));
                lista.add(listaNotas);
            }while (cursor.moveToNext());
        }

        db.close();
        adaptador = new AdaptadorNotas(getActivity().getApplicationContext(), lista);
        notas.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();

        btnFloat= (ImageButton)v.findViewById(R.id.floatAdd);

        btnFloat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = null;
                fragment = new AddNoteFragment();

                getFragmentManager().beginTransaction()
                        .replace(R.id.ContainerLayout, fragment)
                        .commit();

            }
        });

        notas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentTitular = new Intent(getActivity().getApplicationContext(), showDetails.class);
                Notas notas1Seleccionada = (Notas)parent.getItemAtPosition(position);
                Bundle extras = new Bundle();
                extras.putString("Titulo", notas1Seleccionada.getTitle());
                extras.putString("Descripcion", notas1Seleccionada.getDescription());
                intentTitular.putExtras(extras);
                startActivity(intentTitular);
            }
        });

        registerForContextMenu(notas);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_main, menu);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();

        if(v.getId() == R.id.listView){
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            Notas notasTitle = (Notas) notas.getAdapter().getItem(info.position);
            menu.setHeaderTitle(notasTitle.getTitle());
            inflater.inflate(R.menu.menu_ctx_note, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Notas titularMenu = (Notas) notas.getAdapter().getItem(info.position);

        switch (item.getItemId()){
            case R.id.CtxListVer:
                Intent pestañaDetalles = new Intent(getActivity().getApplicationContext(), showDetails.class);
                Bundle extras = new Bundle();
                extras.putString("Titulo", titularMenu.getTitle());
                extras.putString("Descripcion", titularMenu.getDescription());
                pestañaDetalles.putExtras(extras);
                startActivity(pestañaDetalles);
                return true;
            case R.id.CtxListEliminar:
                try {

                    int guardarId = titularMenu.getId();

                    DbHelper notes = new DbHelper(getActivity(), "DBNevermore", null, 1);
                    SQLiteDatabase db = notes.getWritableDatabase();

                    db.execSQL("DELETE FROM Notes WHERE id = '" + guardarId + "'");
                    notas.getAdapter();
                    adaptador.notifyDataSetChanged();

                    Fragment fragment = null;
                    fragment = new NotesActivity();

                    getFragmentManager().beginTransaction()
                            .replace(R.id.ContainerLayout, fragment)
                            .commit();

                } catch (Exception e) {
                    Toast.makeText(getActivity(), "No visible", Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    class AdaptadorNotas extends ArrayAdapter<Notas> {
        public AdaptadorNotas(Context context, ArrayList<Notas> datos) {
            super(context, R.layout.list_notes, datos);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item = inflater.inflate(R.layout.list_notes, null);

            TextView nombreP = (TextView) item.findViewById(R.id.tituloNota);
            TextView descripcionP = (TextView) item.findViewById(R.id.descripcionNota);

            String nombre = lista.get(position).getTitle();
            String description = lista.get(position).getDescription();

            nombreP.setText(nombre);
            descripcionP.setText(description);

            return item;
        }
    }
}
