package com.example.jake.proyecto3bimestre;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jake.proyecto3bimestre.Database.DbHelper;
import com.example.jake.proyecto3bimestre.bean.Idea;
import com.example.jake.proyecto3bimestre.bean.Notas;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FreeActivity extends Fragment {


    private ImageButton agregarHistory;
    private ListView mostrarHistories;

    public FreeActivity() {
        // Required empty public constructor
    }

    final String[] datos = new String[] {"Titulo", "Descripcion"};
    ArrayList<Idea> listaIdeas = new ArrayList<Idea>();
    private AdaptadorIdeas adaptador;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_free, container, false);

        //ListView
        mostrarHistories = (ListView)v.findViewById(R.id.mostrarHistoria);

        //Add history
        agregarHistory = (ImageButton)v.findViewById(R.id.agregarHistoria);

        DbHelper ideaNote = new DbHelper(getActivity().getApplicationContext(), "DBNevermore", null, 1);
        SQLiteDatabase db = ideaNote.getWritableDatabase();

        //Llenar el listview

        Cursor cursor= db.rawQuery("SELECT ideaTitle, descriptionIdea , id FROM Idea", null);
        Idea listIdea;

        if (cursor.moveToFirst()){
            do {
                listIdea = new Idea();
                listIdea.setIdeaName(cursor.getString(0));
                listIdea.setIdeaDescription(cursor.getString(1));
                listIdea.setId(cursor.getInt(2));
                listaIdeas.add(listIdea);
            }while (cursor.moveToNext());
        }

        db.close();
        adaptador = new AdaptadorIdeas(getActivity().getApplicationContext(), listaIdeas);
        mostrarHistories.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();

        agregarHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = null;
                fragment = new free_mode();

                getFragmentManager().beginTransaction()
                        .replace(R.id.ContainerLayout, fragment)
                        .commit();
            }
        });

        mostrarHistories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentTitular = new Intent(getActivity().getApplicationContext(), showDetails.class);
                Idea ideaSeleccionada = (Idea)parent.getItemAtPosition(position);
                Bundle extras = new Bundle();
                extras.putString("Titulo", ideaSeleccionada.getIdeaName());
                extras.putString("Descripcion", ideaSeleccionada.getIdeaDescription());
                intentTitular.putExtras(extras);
                startActivity(intentTitular);
            }
        });

        registerForContextMenu(mostrarHistories);
        return v;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_main, menu);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();

        if(v.getId() == R.id.mostrarHistoria){
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            Idea ideaTitle = (Idea) mostrarHistories.getAdapter().getItem(info.position);
            menu.setHeaderTitle(ideaTitle.getIdeaName());
            inflater.inflate(R.menu.menu_ctx_idea, menu);
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Idea titularMenu = (Idea) mostrarHistories.getAdapter().getItem(info.position);

        switch (item.getItemId()){
            case R.id.CtxListVerIdea:
                Intent pestañaDetalles = new Intent(getActivity().getApplicationContext(), showDetails.class);
                Bundle extras = new Bundle();
                extras.putString("Titulo", titularMenu.getIdeaName());
                extras.putString("Descripcion", titularMenu.getIdeaDescription());
                pestañaDetalles.putExtras(extras);
                startActivity(pestañaDetalles);
                return true;
            case R.id.CtxListEliminarIdea:
                try {

                    int guardarId = titularMenu.getId();

                    DbHelper notes = new DbHelper(getActivity(), "DBNevermore", null, 1);
                    SQLiteDatabase db = notes.getWritableDatabase();

                    db.execSQL("DELETE FROM Idea WHERE id = '" + guardarId + "'");

                    //Recargar nota

                    Fragment fragment = null;
                    fragment = new FreeActivity();

                    getFragmentManager().beginTransaction()
                            .replace(R.id.ContainerLayout, fragment)
                            .commit();

                } catch (Exception e) {
                    Toast.makeText(getActivity(), "No visible", Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    class AdaptadorIdeas extends ArrayAdapter<Idea> {
        public AdaptadorIdeas(Context context, ArrayList<Idea> datos) {
            super(context, R.layout.list_free, datos);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item = inflater.inflate(R.layout.list_free, null);

            TextView nombreP = (TextView) item.findViewById(R.id.tituloModoLibre);
            TextView descripcionP = (TextView) item.findViewById(R.id.descripcionModoLibre);

            String nombre = listaIdeas.get(position).getIdeaName();
            String description = listaIdeas.get(position).getIdeaDescription();

            nombreP.setText(nombre);
            descripcionP.setText(description);

            return item;
        }
    }
}
