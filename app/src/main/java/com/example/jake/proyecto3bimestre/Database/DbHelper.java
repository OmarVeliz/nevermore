package com.example.jake.proyecto3bimestre.Database;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Jake on 12/06/2015.
 */
public class DbHelper extends SQLiteOpenHelper {

    private String sqlNotes = "CREATE TABLE Notes (id INTEGER PRIMARY KEY, title TEXT, description TEXT)";
    private String sqlIdeas = "CREATE TABLE Idea (id INTEGER PRIMARY KEY, ideaTitle TEXT, descriptionIdea TEXT)";
    private String sqlCalendar = "CREATE TABLE Event (id INTEGER PRIMARY KEY, title TEXT,description TEXT,date TEXT,time TEXT)";


    public DbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlNotes);
        db.execSQL(sqlIdeas);
        db.execSQL(sqlCalendar);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //Drop notes

        db.execSQL("DROP TABLE IF EXISTS Notes");
        db.execSQL(sqlNotes);

        //Drop ideas

        db.execSQL("DROP TABLE IF EXISTS Idea");
        db.execSQL(sqlIdeas);

        //Drop Calendar
        db.execSQL("DROP TABLE IF EXISTS Event");
        db.execSQL(sqlCalendar);
    }
}
