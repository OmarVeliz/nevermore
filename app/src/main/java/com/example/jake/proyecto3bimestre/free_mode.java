package com.example.jake.proyecto3bimestre;


import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.jake.proyecto3bimestre.Database.DbHelper;


/**
 * A simple {@link Fragment} subclass.
 */
public class free_mode extends Fragment {


    private EditText ideaName;
    private EditText ideaDescription;
    private ImageButton agregarIdea;

    private String guardarIdea;
    private String guardarDescripcionIdea;

    public free_mode() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_free_mode, container, false);

        ideaName = (EditText)v.findViewById(R.id.nameHistory);
        ideaDescription = (EditText)v.findViewById(R.id.nameHistoryDescription);
        agregarIdea = (ImageButton)v.findViewById(R.id.agregarIdea);

        agregarIdea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                guardarIdea = ideaName.getText().toString();
                guardarDescripcionIdea = ideaDescription.getText().toString();

                DbHelper idea = new DbHelper(getActivity(), "DBNevermore", null, 1);
                SQLiteDatabase db = idea.getWritableDatabase();
                db.execSQL("INSERT INTO Idea VALUES (null, '" + guardarIdea + "', '" + guardarDescripcionIdea + "')");

                ideaName.setText("");
                ideaDescription.setText("");

                Fragment fragment = null;
                fragment = new FreeActivity();

                getFragmentManager().beginTransaction()
                        .replace(R.id.ContainerLayout, fragment)
                        .commit();

                Snackbar.make(v, "Se ha ingresado con exito", Snackbar.LENGTH_LONG)
                        .show();
            }
        });

        return v;
    }


}
