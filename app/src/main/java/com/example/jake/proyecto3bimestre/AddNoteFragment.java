package com.example.jake.proyecto3bimestre;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.jake.proyecto3bimestre.Database.DbHelper;
import com.example.jake.proyecto3bimestre.bean.Notas;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddNoteFragment extends Fragment {

    private EditText titulo;
    private EditText nota;
    private ImageButton aceptar;

    //Guardar pelicula

    private String tituloNota;
    private String descripcionNota;
    private static final int NOTIF_ALERTA_ID = 1;

    public AddNoteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_add_note, container, false);

        titulo = (EditText)v.findViewById(R.id.EditTitulo);
        nota = (EditText)v.findViewById(R.id.descripcion);
        aceptar = (ImageButton)v.findViewById(R.id.aceptar);

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tituloNota = titulo.getText().toString();
                descripcionNota = nota.getText().toString();

                DbHelper notes = new DbHelper(getActivity(), "DBNevermore", null, 1);
                SQLiteDatabase db = notes.getWritableDatabase();

                db.execSQL("INSERT INTO Notes VALUES (null, '" + tituloNota + "', '" + descripcionNota + "')");

                //Notificacion

                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(getActivity())
                                .setSmallIcon(android.R.drawable.stat_sys_warning)
                                .setLargeIcon((((BitmapDrawable)getResources()
                                        .getDrawable(R.drawable.ocaso)).getBitmap()))
                                .setContentTitle("Nota")
                                .setContentText("Descripcion")
                                .setContentInfo("Nota")
                                .setTicker("Nota!");

                Intent pestañaNotas = new Intent(getActivity(), NotesActivity.class);

                PendingIntent contIntent =
                        PendingIntent.getActivity(getActivity(), 0, pestañaNotas, 0);

                mBuilder.setContentIntent(contIntent);

                //Notificar el acto

                NotificationManager mNotificationManager =
                        (NotificationManager) getActivity().getApplicationContext().getSystemService(getActivity().getApplicationContext().NOTIFICATION_SERVICE);

                mNotificationManager.notify(NOTIF_ALERTA_ID, mBuilder.build());

                titulo.setText("");
                nota.setText("");

                Fragment fragment = null;
                fragment = new NotesActivity();

                getFragmentManager().beginTransaction()
                        .replace(R.id.ContainerLayout, fragment)
                        .commit();

                Snackbar.make(v, "Se ha ingresado con exito", Snackbar.LENGTH_LONG)
                        .show();

            }
        });

        return v;

    }


}
