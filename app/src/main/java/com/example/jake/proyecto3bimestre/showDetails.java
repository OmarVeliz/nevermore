package com.example.jake.proyecto3bimestre;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class showDetails extends ActionBarActivity {


    private Toolbar mToolbar;
    private TextView nombreNotaDetalle;
    private TextView descripcionNotaDetalle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_details);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nombreNotaDetalle = (TextView)findViewById(R.id.nombreNotaDetalle);
        descripcionNotaDetalle = (TextView)findViewById(R.id.descripcionNotaDetalle);

        Bundle extras = getIntent().getExtras();

        nombreNotaDetalle.setText(extras.getString("Titulo"));
        descripcionNotaDetalle.setText(extras.getString("Descripcion"));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {
            showDetails.this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
