package com.example.jake.proyecto3bimestre;


import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jake.proyecto3bimestre.Database.DbHelper;


/**
 * A simple {@link Fragment} subclass.
 */
public class EventFragment extends Fragment {

    private TextView lblDate, txtTitle, txtDescription;
    private EditText txtHour, txtMin;
    private String time;

    public EventFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_event, container, false);

        //Dates Loading
        txtHour = (EditText) v.findViewById(R.id.txtHour);
        txtMin = (EditText) v.findViewById(R.id.txtMins);
        txtTitle = (TextView) v.findViewById(R.id.txtTitulo);
        txtDescription = (TextView) v.findViewById(R.id.txtDescription);
        lblDate = (TextView) v.findViewById(R.id.lblDate);


        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("DatosCalendar", Context.MODE_PRIVATE);
        String dia = pref.getString("Dia", null);

        lblDate.setText(dia);

        //End

        return v;
    }

    public void eventClick(View view) {
        time = txtHour.getText().toString() + ":" + txtMin.getText().toString();

        if (txtTitle.getText().toString().equals("")) {
            Toast.makeText(getActivity(), "Title", Toast.LENGTH_LONG).show();
        } else {
            if (txtDescription.getText().toString().equals("")) {
                Toast.makeText(getActivity(), "Description", Toast.LENGTH_LONG).show();
            } else {
                if (txtHour.getText().toString().equals("") || txtMin.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Time", Toast.LENGTH_LONG).show();
                } else {
                    DbHelper eventSQLiteHelper = new DbHelper(getActivity().getApplicationContext(), "DBNeverMore", null, 1);
                    SQLiteDatabase db = eventSQLiteHelper.getWritableDatabase();

                    ContentValues newRegister = new ContentValues();
                    newRegister.put("title", txtTitle.getText().toString());
                    newRegister.put("description", txtDescription.getText().toString());
                    newRegister.put("date", lblDate.getText().toString());
                    newRegister.put("time", time);
                    db.insert("Event", "", newRegister);

                    db.close();
                    Toast.makeText(getActivity(), "Complete", Toast.LENGTH_LONG).show();
                    txtTitle.setText("");
                    txtDescription.setText("");
                    txtHour.setText("");
                    txtMin.setText("");

                }
            }
        }
    }
}