package com.example.jake.proyecto3bimestre.bean;

/**
 * Created by Jake on 13/06/2015.
 */
public class Idea {

    private int id;
    private String ideaName;
    private String ideaDescription;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdeaName() {
        return ideaName;
    }

    public String getIdeaDescription() {
        return ideaDescription;
    }

    public void setIdeaName(String ideaName) {
        this.ideaName = ideaName;
    }

    public void setIdeaDescription(String ideaDescription) {
        this.ideaDescription = ideaDescription;
    }
}
